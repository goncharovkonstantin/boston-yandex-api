package com.none;

import java.util.Scanner;

public class Core {

    public static void main(String[] args)
    {
        RequestHandler rHandler = new RequestHandler();
        Scanner in = new Scanner(System.in);
        while (true)
        {
            System.out.println("Enter a phrase you want to translate, type 'q' to exit");
            String phrase = in.nextLine();
            if (phrase.trim().equals("")) {
                System.out.println("Empty sentences can't be translated");
                continue;
            }
            if (phrase.equals("q"))
                break;
            else
                System.out.println(rHandler.translate(phrase));
        }
    }
}
